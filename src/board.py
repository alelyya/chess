class Board():
    def __init__(self,height,width):
        self.height = height
        self.width = width
        self.squares = [[None for _ in range(self.width)] for _ in range(self.height)]
    
    def __getitem__(self,key):
        return self.squares[key]
    
    def __setitem__(self,key,value):
        self.squares[key] = value

    def __iter__(self):
        for i in self.squares:
            yield i
