from info import Kind, Color
from piece import Piece
from player import Move
import copy


class Rules():
    
    def StartingPosition(board, setup=0):
        if setup == 0:
            board[0][0],board[0][7] = Piece(Kind.ROOK,Color.BLACK),Piece(Kind.ROOK,Color.BLACK)
            board[0][1],board[0][6] = Piece(Kind.KNIGHT,Color.BLACK),Piece(Kind.KNIGHT,Color.BLACK)
            board[0][2],board[0][5] = Piece(Kind.BISHOP,Color.BLACK),Piece(Kind.BISHOP,Color.BLACK)
            board[0][3] = Piece(Kind.QUEEN,Color.BLACK)
            board[0][4] = Piece(Kind.KING,Color.BLACK)
            for square in range(8):
                board[1][square] = Piece(Kind.PAWN,Color.BLACK)
                board[6][square] = Piece(Kind.PAWN,Color.WHITE)
            board[7][0],board.squares[7][7] = Piece(Kind.ROOK,Color.WHITE),Piece(Kind.ROOK,Color.WHITE)
            board[7][1],board.squares[7][6] = Piece(Kind.KNIGHT,Color.WHITE),Piece(Kind.KNIGHT,Color.WHITE)
            board[7][2],board.squares[7][5] = Piece(Kind.BISHOP,Color.WHITE),Piece(Kind.BISHOP,Color.WHITE)
            board[7][3] = Piece(Kind.QUEEN,Color.WHITE)
            board[7][4] = Piece(Kind.KING,Color.WHITE)
    
    def IsLegal(board, move, player, enemy):
        if Rules.PseudoLegal(board, move, player, enemy):
            if Rules.ClearPath(board, move):
                copyboard = copy.deepcopy(board)
                copyplayer = copy.deepcopy(player)
                copyenemy = copy.deepcopy(enemy)
                Rules.MakeMove(copyboard, move, copyplayer)
                #Rules.UpdateAvailable(copyboard, copyenemy, copyplayer) # INF.LOOP
                if not Rules.Attacked(copyboard, copyplayer.king_position, copyenemy):
                    return True
        return False
            
    def Attacked(board, piece_pos, enemy):
        for move in enemy.available_moves:
            end_square = move.End
            if end_square == piece_pos:
                return True
        return False

    def IsCheckmate(board):
        return False
    
    def IsStalemate(board):
        return False
    
    def ClearPath(board, move):

        from_row, from_col = move.Init[0], move.Init[1]
        to_row, to_col = move.End[0], move.End[1]

        if board[from_row][from_col].Kind == Kind.KNIGHT:
            return True

        step_r = sign(to_row-from_row)
        step_c = sign(to_col-from_col)

        while not (from_row==to_row-step_r and from_col==to_col-step_c):
            from_row+=step_r
            from_col+=step_c
            if board[from_row][from_col] is not None:
                return False
        return True

    def AllAvailable(board, player, enemy):
        allav = []
        for row in range(board.height):
            for col in range(board.width):
                if player.pieces[row][col]:
                    piece_moves = Rules.AvailableMoves(board, (row,col), player, enemy)
                    allav.extend(piece_moves)
        return allav

    def UpdateAvailable(board, player, enemy):
        player.available_moves = Rules.AllAvailable(board, player, enemy)

    def SquareGenerator(piece, row, col):
        possible_squares = []

        def QueenSquares(row, col):
            squares = []
            for step in range(1,8):
                squares.extend([(row+step, col+step), (row-step, col-step), (row+step, col-step), (row-step, col+step),
                                (row-step, col), (row+step, col), (row, col-step), (row, col+step)])
            return squares
        
        def KnightSquares(row, col):
            return [(row-2,col+1),(row-1,col+2),(row+1,col+2),(row+2,col+1),
                    (row+2,col-1),(row+1,col-2),(row-1,col-2),(row-2,col-1)]

        if piece.Kind == Kind.PAWN:
            step = 1
            if piece.Color == Color.WHITE:
                step =-1
            possible_squares = [(row+step,col-1), (row+step,col+1), (row+step,col), (row+step*2,col)]
        
        elif piece.Kind == Kind.ROOK:
            for step in range(1,8):
                possible_squares.extend([(row-step, col), (row+step, col), (row, col-step), (row, col+step)])
                
        elif piece.Kind == Kind.KNIGHT:
            possible_squares = KnightSquares(row, col)
        
        elif piece.Kind == Kind.BISHOP:
            for step in range(1,8):
                possible_squares.extend([(row+step, col+step), (row-step, col-step),
                                         (row+step, col-step), (row-step, col+step)])
        
        elif piece.Kind == Kind.QUEEN:
            possible_squares = QueenSquares(row, col)
        
        elif piece.Kind == Kind.KING:
            possible_squares = [(row-1,col),(row-1,col+1),(row,col+1),(row+1,col+1),(row+1,col),(row+1,col-1),
                                (row,col-1),(row-1,col-1),(row,col-2),(row,col+2)]
        elif piece.Kind == Kind.AMAZON:
            possible_squares = QueenSquares(row, col).extend(KnightSquares(row, col))
        return possible_squares

    def AvailableMoves(board, init_pos, player, enemy):
        row = init_pos[0]
        col = init_pos[1]
        moves = []

        if not (0<=row<board.height and 0<=col<board.width):
            return moves

        piece = board[row][col]

        if piece is not None:
            if piece.Color == player.color:
                for square in Rules.SquareGenerator(piece, row, col):
                    move = player.Move([row, col],[square[0], square[1]])
                    if Rules.IsLegal(board, move, player, enemy):
                        moves.append(move)
        return moves

    def PseudoLegal(board, move, player, enemy):

        from_row = move.Init[0]
        from_col = move.Init[1]
        to_row = move.End[0]
        to_col = move.End[1]

        if to_col>board.width-1 or to_row>board.height-1 or to_row<0 or to_col<0:
            return False

        row_diff = to_row - from_row
        col_diff = to_col - from_col

        piece = board[from_row][from_col]
        aim = board[to_row][to_col]

        if piece is not None:
            if move.PlayerColor!=piece.Color:
                return False
            else:
                if aim is None or aim.Color != piece.Color:

                    if piece.Kind == Kind.ROOK:
                        if (from_row == to_row or from_col == to_col) and Rules.ClearPath(board,move):
                            return True
                        else:
                            return False

                    if piece.Kind == Kind.KNIGHT:
                        if row_diff == -2 and col_diff == 1:
                            return True
                        if row_diff == -1 and col_diff == 2:
                            return True
                        if row_diff == 1 and col_diff == 2:
                            return True
                        if row_diff == 2 and col_diff == 1:
                            return True
                        if row_diff == -2 and col_diff == -1:
                            return True
                        if row_diff == -1 and col_diff == -2:
                            return True
                        if row_diff == 1 and col_diff == -2:
                            return True
                        if row_diff == 2 and col_diff == -1:
                            return True
                        else:
                            return False

                    if piece.Kind == Kind.BISHOP:
                        if abs(row_diff) == abs(col_diff) and Rules.ClearPath(board,move):
                            return True
                        else:
                            return False

                    if piece.Kind == Kind.QUEEN:
                        if (abs(row_diff) == abs(col_diff) or from_row == to_row or from_col == to_col) and Rules.ClearPath(board,move):
                            return True
                        else:
                            return False

                    if piece.Kind == Kind.KING:
                        if abs(row_diff)<=1 and abs(col_diff)<=1:
                            return True
                        elif row_diff == 0 and abs(col_diff) == 2:
                            if col_diff == 2:
                                STEP = 1
                                if not player.cstl_srt:
                                    return False
                                else:
                                    end = [to_row, 7]
                            else:
                                STEP = -1
                                if not player.cstl_lng:
                                    return False
                                end = [to_row, 0]
                         
                            move = Move([from_row, from_col], end, player.color)
                            second_piece = board[end[0]][end[1]]
                            if second_piece is not None:
                                if second_piece.Kind == Kind.ROOK and second_piece.Color == player.color:
                                    if Rules.ClearPath(board, move):
                                        one = Rules.Attacked(board,[from_row, from_col],enemy)
                                        two = Rules.Attacked(board,[from_row, from_col+STEP],enemy)
                                        three = Rules.Attacked(board,[from_row, from_col+STEP*2], enemy)
                                        if not any((one, two, three)):
                                            return True
                        else:
                            return False

                    if piece.Kind == Kind.PAWN:
                        if piece.Color == Color.WHITE:
                            if from_col == to_col and board[to_row][to_col] is None:
                                if from_row == board.height-2:
                                    if (row_diff==-2 or row_diff==-1) and Rules.ClearPath(board,move):
                                        return True
                                else:
                                    if row_diff==-1:
                                        return True
                            else:
                                if abs(col_diff)==1 and row_diff==-1 and board[to_row][to_col] is not None:
                                    return True
                                else:
                                    return False
                        elif piece.Color == Color.BLACK:
                            if from_col == to_col and board[to_row][to_col] is None:
                                if from_row == 1:
                                    if (row_diff==2 or row_diff==1) and Rules.ClearPath(board,move):
                                        return True
                                else:
                                    if row_diff==1:
                                        return True
                            else:
                                if abs(col_diff)==1 and row_diff==1 and board[to_row][to_col] is not None:
                                    return True
                                else:
                                    return False
                else:
                    return False

    def MakeMove(board, move, player):
        from_row = move.Init[0]
        from_col = move.Init[1]
        to_row = move.End[0]
        to_col = move.End[1]

        piece = board[from_row][from_col]
        
        if piece.Kind == Kind.ROOK:
            if from_col == 7:
                player.cstl_srt = False
            elif from_col == 0:
                player.cstl_lng = False
        
        if piece.Kind == Kind.KING:
            Rules.KingMove(board, move, player)
            return board

        player.pieces[from_row][from_col], player.pieces[to_row][to_col] = 0, 1
        board[from_row][from_col],board[to_row][to_col] = None, piece
        return board

    def KingMove(board, move, player):
        player.cstl_lng, player.cstl_srt = False, False
        
        from_row = move.Init[0]
        from_col = move.Init[1]
        to_row = move.End[0]
        to_col = move.End[1]

        KING = board[from_row][from_col]
        
        if from_row in (0, 7) and from_col == 4 and to_row == from_row and to_col in (2, 6):
        
                if to_col > from_col:
                    ROOK = board[from_row][7]
                    board[from_row][from_col+1], board[from_row][7] = ROOK, None
                    player.pieces[from_row][7], player.pieces[from_row][5] = 0, 1
                else:
                    ROOK = board[from_row][0]
                    board[from_row][from_col-1], board[from_row][0] = ROOK, None
                    player.pieces[from_row][0], player.pieces[from_row][2] = 0, 1
        
        board[from_row][from_col],board[to_row][to_col] = None, KING
        player.pieces[from_row][from_col], player.pieces[to_row][to_col] = 0, 1

        player.king_position = [to_row, to_col]
        return board

    def MoveToPGN(board, move):
        from_row = move.Init[0]
        from_col = move.Init[1]
        to_row = move.End[0]
        to_col = move.End[1]

        init_file = chr(from_col+97)
        init_rank = int(board.height - from_row)
        end_file = chr(to_col+97)
        end_rank = int(board.height - to_row)
        from_square = init_file + str(init_rank)
        to_square = end_file + str(end_rank)
        
        piece = board[from_row][from_col]
        target = board[to_row][to_col]

        if target is not None:
            sep = 'x'
        else:
            sep = ''

        if piece.Kind == Kind.ROOK:
            PGN_move = 'R' + sep + to_square
        elif piece.Kind == Kind.KNIGHT:
            PGN_move = 'N' + sep + to_square
        elif piece.Kind == Kind.BISHOP:
            PGN_move = 'B' + sep + to_square
        elif piece.Kind == Kind.QUEEN:
            PGN_move = 'Q' + sep + to_square
        elif piece.Kind == Kind.KING:
            if from_square in ('e1', 'e8') and to_square in ('g8','g1'):
                PGN_move = 'O-O'
            elif from_square in ('e1', 'e8') and to_square in ('c8','c1'):
                PGN_move = 'O-O-O'
            else:
                PGN_move = 'K' + sep + to_square
        elif piece.Kind == Kind.PAWN:
            if target:
                PGN_move = from_square[0] + sep + to_square
            else:
                PGN_move = to_square
        return PGN_move


def sign(x):
    return(x and (1, -1)[x<0])
