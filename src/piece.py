class Piece(object):
    def __init__(self, kind,color):
        self.kind = kind
        self.color = color

    @property
    def Kind(self):
        return self.kind
    @property
    def Color(self):
        return self.color
    def GetSymbol(self):
        symbol_list = {1 : {1:'♖',2:'♘',3:'♗',4:'♕',5:'♔',6:'♙'},
                       2: {1:'♜',2:'♞',3:'♝',4:'♛',5:'♚',6:'♟'}}
        return symbol_list[self.color][self.kind]
    def GetName(self):
        name_list = {1 : {1:'wR',2:'wN',3:'wB',4:'wQ',5:'wK',6:'wP'},
                     2: {1:'bR',2:'bN',3:'bB',4:'bQ',5:'bK',6:'bP'}}
        return name_list[self.color][self.kind]
