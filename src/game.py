
from board import Board
from player import Player
from rules import Rules
from history import History, HistoryElement
from info import Color
import copy, sys

from Qt import Ui_MainWindow
from PyQt5.QtWidgets import QApplication


class Game():
    def __init__(self, Essence1, Essence2):
        
        self.history = History()
        self.p1 = Player(Color.WHITE, Essence1)
        self.p2 = Player(Color.BLACK, Essence2)
        
        self.ply = 1
        self.board = Board(8, 8)

        self.p1.pieces = [[0]*8]*6+[[1]*8]*2
        self.p2.pieces = [[1]*8]*2+[[0]*8]*6
        Rules.StartingPosition(self.board)

        self.MainLoop()

    def MainLoop(self):

        self.app = QApplication(sys.argv)
        main = Ui_MainWindow(self.board, self.p1, self.p2, self.ply, 1024, 696)
        main.show()
        sys.exit(self.app.exec_())

    def __Record(self, move, PGN_move):
        board = copy.deepcopy(self.board)
        p1 = copy.deepcopy(self.p1)
        p2 = copy.deepcopy(self.p2)
        ply = copy.copy(self.ply)
        self.history.Push(HistoryElement(board, p1, p2, ply, PGN_move))


class Essence():
    HUMAN = 1
    AI = 2
