from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QMainWindow, QGraphicsPixmapItem, QSlider
from PyQt5 import QtSvg, QtCore, QtGui
from rules import Rules
import os


class GraphicsBoard(QtCore.QObject):

    updated = QtCore.pyqtSignal(int, int, int, int)
    clicked = QtCore.pyqtSignal(int, int)

    basePath = os.path.dirname(__file__)
    dotPath = os.path.join(basePath,"logic/dot.png")
    squarePath = os.path.join(basePath,"logic/square.png")

    RPathALPHA = [os.path.join(basePath,"piece/ALPHA/wR.svg"), os.path.join(basePath,"piece/ALPHA/bR.svg")]
    NPathALPHA = [os.path.join(basePath,"piece/ALPHA/wN.svg"), os.path.join(basePath,"piece/ALPHA/bN.svg")]
    BPathALPHA = [os.path.join(basePath,"piece/ALPHA/wB.svg"), os.path.join(basePath,"piece/ALPHA/bB.svg")]
    QPathALPHA = [os.path.join(basePath,"piece/ALPHA/wQ.svg"), os.path.join(basePath,"piece/ALPHA/bQ.svg")]
    KPathALPHA = [os.path.join(basePath,"piece/ALPHA/wK.svg"), os.path.join(basePath,"piece/ALPHA/bK.svg")]
    PPathALPHA = [os.path.join(basePath,"piece/ALPHA/wP.svg"), os.path.join(basePath,"piece/ALPHA/bP.svg")]
    ALPHA = [RPathALPHA, NPathALPHA, BPathALPHA, QPathALPHA, KPathALPHA, PPathALPHA]

    darkPathGREY = os.path.join(basePath,"board/GREY/darksquares.jpg")
    lightPathGREY = os.path.join(basePath,"board/GREY/lightsquares.jpg")

    darkPathBROWN = os.path.join(basePath,"board/BROWN/darksquares.png")
    lightPathBROWN = os.path.join(basePath,"board/BROWN/lightsquares.png")

    def __init__(self, scene, board, BOARD_SIZE, parent = None):
        super(GraphicsBoard, self).__init__(parent)
        self.scene = scene
        self.board = board
        self.board_size = BOARD_SIZE
        self.SQ_SIZE = BOARD_SIZE / (board.height)
        
        self.initUI()
        
    def initUI(self):

        self.reset()
        self.SetBoard(Style.BROWN, Style.ALPHA)

    def SetAvailable(self, moves):
        for move in moves:
            if self.board[move.End[0]][move.End[1]] is not None:
                img = QtGui.QPixmap(GraphicsBoard.squarePath)
            else:
                img = QtGui.QPixmap(GraphicsBoard.dotPath)
            sc_img = img.scaled(self.SQ_SIZE, self.SQ_SIZE)
            pixmap = QGraphicsPixmapItem(sc_img)
            pixmap.setPos(move.End[1]*self.SQ_SIZE, move.End[0]*self.SQ_SIZE)
            self.scene.addItem(pixmap)

    def SetBoard(self, board_style, piece_style):
        if board_style == Style.GREY:
            dark = QtGui.QPixmap(GraphicsBoard.darkPathGREY)
            light = QtGui.QPixmap(GraphicsBoard.lightPathGREY)
        elif board_style == Style.BROWN:
            dark = QtGui.QPixmap(GraphicsBoard.darkPathBROWN)
            light = QtGui.QPixmap(GraphicsBoard.lightPathBROWN)
        if piece_style == Style.ALPHA:
            PIECESET = GraphicsBoard.ALPHA

        sc_dark = dark.scaled(self.SQ_SIZE, self.SQ_SIZE)
        sc_light = light.scaled(self.SQ_SIZE, self.SQ_SIZE)

        for row_num, row in enumerate(self.board):
            for col_num, piece in enumerate(row):
                if (row_num+col_num) % 2 != 0:
                    pix = QGraphicsPixmapItem(sc_dark)
                    pix.setPos(row_num*self.SQ_SIZE, col_num*self.SQ_SIZE)
                    self.scene.addItem(pix)
                    pix.setZValue(0)
                else:
                    pix = QGraphicsPixmapItem(sc_light)
                    pix.setPos(row_num*self.SQ_SIZE, col_num*self.SQ_SIZE)
                    self.scene.addItem(pix)
                    pix.setZValue(0)
                if piece is not None:
                    gf_piece = GraphicsPiece(PIECESET[piece.Kind-1][piece.Color-1], self)
                    gf_piece.square_size = self.SQ_SIZE
                    gf_piece.setPos(col_num*self.SQ_SIZE, row_num*self.SQ_SIZE)
                    gf_piece.init_square = (gf_piece.pos().y() // self.SQ_SIZE, gf_piece.pos().x() // self.SQ_SIZE)
                    factor = self.board_size / (self.board.height*gf_piece.boundingRect().height())
                    gf_piece.setScale(factor)
                    gf_piece.setZValue(1)
                    self.scene.addItem(gf_piece)

    def reset(self):
        for item in self.scene.items():
            self.scene.removeItem(item)


class GraphicsPiece(QtSvg.QGraphicsSvgItem):
    def __init__(self, str, board, parent = None):
        super().__init__(str, parent)

        self.setFlag(QtSvg.QGraphicsSvgItem.ItemIsMovable, True)
        #self.setFlag(QtSvg.QGraphicsSvgItem.ItemIsSelectable, True)
        self.setFlag(QtSvg.QGraphicsSvgItem.ItemSendsGeometryChanges, True)
        self.board = board

    def mouseMoveEvent(self, event):
        mouse_pos = event.scenePos()
        x = mouse_pos.x()-self.square_size/2
        y = mouse_pos.y()-self.square_size/2
        self.setPos(x,y)

    def mousePressEvent(self, event):
        mouse_pos = event.scenePos()
        x = mouse_pos.x()-self.square_size/2
        y = mouse_pos.y()-self.square_size/2
        self.setZValue(65)
        self.setPos(x,y)
        self.board.clicked.emit(self.init_square[0], self.init_square[1])
        QtSvg.QGraphicsSvgItem.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        self.setZValue(1)
        mouse_pos = event.scenePos()
        x = mouse_pos.x()
        y = mouse_pos.y()
        move = (int(self.init_square[0]), int(self.init_square[1]),
                int(x // self.square_size), int(y // self.square_size))
        self.board.updated.emit(move[0], move[1], move[2], move[3])
        QtSvg.QGraphicsSvgItem.mouseReleaseEvent(self, event)


class Ui_MainWindow(QMainWindow):
    def __init__(self, model_board, player, enemy, ply, SCREENWIDTH, SCREENHEIGHT):
        super().__init__()
        self.centralwidget = QWidget(self)

        self.model_board = model_board
        self.player = player
        self.enemy = enemy
        self.ply = ply

        self.BOARD_SIZE = 512   # %8==0 for better result

        slider = QSlider(QtCore.Qt.Vertical, self.centralwidget)
        self.scale_step = 8     # %8==0 for better result
        slider.setRange(2, 75)
        slider.setValue(self.BOARD_SIZE // self.scale_step)
        slider.setFocusPolicy(QtCore.Qt.NoFocus)
        slider.setGeometry(SCREENWIDTH-40, 25, 15, 150)
        slider.valueChanged[int].connect(self.BoardResize)

        self.setCentralWidget(self.centralwidget)
        self.setWindowTitle('Chess')
        self.resize(SCREENWIDTH, SCREENHEIGHT)
        
        self.board_scene = QGraphicsScene(self.centralwidget)
        self.graphics_board = GraphicsBoard(self.board_scene, model_board, self.BOARD_SIZE)
        
        self.board_view = QGraphicsView(self.board_scene, self.centralwidget)
        self.board_view.setGeometry(QtCore.QRect(25, 25, self.BOARD_SIZE, self.BOARD_SIZE))
        self.board_view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.board_view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.Process()

    def BoardResize(self, value):
        value = value * self.scale_step
        self.graphics_board.board_width, self.graphics_board.board_size = value, value
        self.graphics_board.SQ_SIZE = value // (self.model_board.height)
        self.board_view.resize(value, value)
        self.graphics_board.initUI()

    def Process(self):
        @QtCore.pyqtSlot(int, int, int, int)
        def Update_Board(y1, x1, x2, y2):
            move = self.player.Move((y1,x1), (y2, x2))
            if Rules.IsLegal(self.model_board, move, self.player, self.enemy):
                Rules.MakeMove(self.model_board, move, self.player)
                self.player, self.enemy = self.enemy, self.player
                #Rules.UpdateAvailable(self.model_board, self.enemy, self.player)
                #Rules.UpdateAvailable(self.model_board, self.player, self.enemy)
                self.ply+=1
            self.graphics_board.initUI()

        @QtCore.pyqtSlot(int, int)
        def Update_Available(y,x):
            moves = Rules.AvailableMoves(self.model_board, (y,x), self.player, self.enemy)
            self.graphics_board.SetAvailable(moves)
        
        self.graphics_board.updated.connect(Update_Board)
        self.graphics_board.clicked.connect(Update_Available)


class Style():
    GREY = 1
    BROWN = 2
    ALPHA = 10
