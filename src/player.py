from info import Color


class Move():
    def __init__(self,init_pos,end_pos,player_color):
        self.init_pos = init_pos
        self.end_pos = end_pos
        self.player_color = player_color
        self.move = (init_pos,end_pos)
    
    @property
    def Init(self):
        return self.init_pos
    @property
    def End(self):
        return self.end_pos
    @property
    def PlayerColor(self):
        return self.player_color


class Player():
    def __init__(self, color, essence):
        self.color = color
        self.esence = essence
        
        self.available_moves = []
        self.cstl_lng = True
        self.cstl_srt = True
        self.king_position = [7, 4]
        if self.color == Color.BLACK:
            self.king_position = [0, 4]

    def Move(self,init_pos, end_pos):
        return Move(init_pos,end_pos,self.color)
