
class History():
    def __init__(self):
        self.data = []
        self.point = 0

    def Push(self, item):
        if self.point != 0:
            item.parent = self.data[self.point-1]
            self.data[self.point-1].children.append(item)
        self.data.insert(self.point, item)
        self.Forward()

    def Current(self):
        return self.data[self.point-1]

    def Back(self):
        if self.point > 0:
            self.point -= 1

    def Forward(self):
        if self.point < len(self.data):
            self.point += 1

    def GetBranch(self):
        branch = []
        item = self.data[self.pointer]
        branch.append(item)
        while item.children:
            branch.append(item.children)
            item = item.children
        return branch

    def ReturnPGN(self):
        PGN_list = []
        for item in self.data:
            PGN_list.append(item.PGN_move)
        return PGN_list


class HistoryElement():
    def __init__(self, board, p1, p2, ply, PGN_move):
        self.parent = []
        self.children = []
        self.board = board
        self.p1 = p1
        self.p2 = p2
        self.ply = ply
        self.PGN_move = PGN_move
